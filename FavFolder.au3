If $CmdLine[0] > 2 Then Exit
If $CmdLine[1] == "help" Then
	MsgBox(0, "FavFolder Utility Usage", "FavFolder.exe <save/load> <#> where <#> is the number of the saved folder. Note that the saved file will be in the directory of execution.")
	Exit
EndIf

;~ $file = "D:\Program Files\Launchy\Utilities\Current Folder\" & "FavFolder" & $CmdLine[2] & ".txt"

If $CmdLine[0] == 2 Then
	$file = @WorkingDir & "\FavFolder" & $CmdLine[2] & ".txt"
	If $CmdLine[1] == "save" Then
		WinWaitClose("Launchy", "input")
		$str = _getPath()
		FileDelete($file)
		FileWrite($file, $str)
	EndIf

	If $CmdLine[1] == "openExp" Then
		If FileExists($file) Then
			$FolderPath = FileRead($file)
			Run("Explorer.exe " & $FolderPath)
		Else
			Exit
		EndIf
	EndIf

	If $CmdLine[1] == "openCmd" Then
		If FileExists($file) Then
			$FolderPath = FileRead($file)
			Run('cmd.exe /k cd "' & $FolderPath & '" & ' & StringLeft($FolderPath, 2))
		Else
			Exit
		EndIf
	EndIf

Else
	If $CmdLine[1] == "cmdHere" Then
		WinWaitClose("Launchy", "input")
		$path = _getPath()
		Run('cmd.exe /k cd "' & $path & '" & ' & StringLeft($path, 2))
	EndIf
EndIf

Func _getPath()
		$str = WinGetText("")
		ClipPut(WinGetText(""))

		$ar = StringRegExp($str, "(:?Address: )(.*)\v", 3)

		if (UBound($ar,1) < 2) Then Exit
		if (_FilePathIsValid($ar[1]) <> 1) Then Exit

	    return $ar[1]
EndFunc

Func _FilePathIsValid($sPath)
    Local $sInvalid_Chars = '*?|:<>"/'
    Local $sPattern = '(?i)^[a-z]:([^\Q' & $sInvalid_Chars & '\E]+)?(\\[^\Q' & $sInvalid_Chars & '\E\\]+)?(\.[^\Q' & $sInvalid_Chars & '\E\\\.]+|\\)?$'
    Local $iPathIsValid = StringRegExp($sPath, $sPattern)

    Return $iPathIsValid
EndFunc