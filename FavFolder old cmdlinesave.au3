If $CmdLine[0] > 2 Then Exit
If $CmdLine[1] == "help" Then
	MsgBox(0,"FavFolder Utility Usage", "FavFolder.exe <save/load> <#> where <#> is the number of the saved folder. Note that the saved file will be in D:\Program Files\Launchy\Utilities\Current Folder")
	Exit
EndIf
$str = ClipGet()
$file = "D:\Program Files\Launchy\Utilities\Current Folder\" & "FavFolder" & $CmdLine[2] & ".txt"
If $CmdLine[1] == "save" Then
	If StringInStr($str, ":\") and StringLeft($str,1) <> "h" Then
		FileDelete($file)
		FileWrite($file,$str)
	EndIf
EndIf
If $CmdLine[1] == "load" Then
	If FileExists($file) Then
		$FolderPath = FileRead($file)
		Run("Explorer.exe " & $FolderPath)
	Else
		Exit
	EndIf
EndIf

